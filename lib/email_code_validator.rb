class EmailCodeValidator
  def self.generate(email)
    secret = ENV['SECRET_KEY_BASE']
    key = [ email, secret ].join('|')
    digest = Digest::SHA256.hexdigest(key)
    Base64.encode64(email) + '|' + digest
  end

  def self.validate(token)
    email_encoded, digest = token.to_s.split('|')
    if email_encoded && digest
      email = Base64.decode64(email_encoded)
      (token == generate(email)) ? email : nil
    else
      nil
    end
  end

end
