var { Router, Route, IndexRoute, IndexLink, Link } = ReactRouter;

var Welcome = React.createClass({
  componentDidMount: function() {
    var input = $('input[name=date]', this._root);
    input.datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
      this.handleDateChange(e);
    }.bind(this));
  },
  getInitialState: function() {
    return { date: '2016-09-01' };
  },
  handleSubmit: function(e) {
    e.preventDefault();
    console.log('Select date: navigating to comments');
    this.props.history.push('/dashboard/comments?' + this.state.date);
  },
  handleDateChange: function(e) {
    console.log('date changed');
    this.setState({date: e.target.value});
  },
  render: function() {
    var self = this;
    return (
      <div ref={function(e) { self._root = e; }}>
        <h1>Welcome</h1>
          <form id="form-select-date" onSubmit={this.handleSubmit}>
            <h3>Select Date</h3>
            <div className="form-group">
              <input name="date" type="text" value={this.state.date}
                     onChange={this.handleDateChange}/>
            </div>
            <button className="btn btn-success">
              <span>Set Date</span>&nbsp;<span
              className="glyphicon glyphicon-ok" aria-hidden="true"></span>
            </button>
          </form>
      </div>
    )
  }
});

var Comment = React.createClass({

  rawMarkup: function() {
    var md = new Remarkable();
    var rawMarkup = md.render(this.props.children.toString());
    return { __html: rawMarkup };
  },

  render: function() {
    return (
      <li className="comment">
        <h2 className="commentAuthor">
          {this.props.author}
        </h2>
        <span dangerouslySetInnerHTML={this.rawMarkup()}/>
      </li>
    )
  }
});

var CommentList = React.createClass({
  render: function() {
    var commentNodes = this.props.data.map(function(comment) {
      return (
        <Comment author={comment.author} key={comment.id}>
          {comment.text}
        </Comment>
      )
    });
    return (
      <ul className="commentList">
        {commentNodes}
      </ul>
    );
  }
});

var CommentForm = React.createClass({
  getInitialState: function() {
    return { author: '', text: '' };
  },
  handleAuthorChange: function(e) {
    this.setState({author: e.target.value});
  },
  handleTextChange: function(e) {
    this.setState({text: e.target.value});
  },
  handleSubmit: function(e) {
    e.preventDefault();
    var author = this.state.author.trim();
    var text = this.state.text.trim();
    if (!author || !text) {
      return;
    }
    this.props.onCommentSubmit({author: author, text: text});
    this.setState({ author: '', text: '' });
  },
  render: function() {
    return (
      <form className="commentForm" onSubmit={this.handleSubmit}>
        <input type="text" placeholder="Your name"
               value={this.state.author} onChange={this.handleAuthorChange}/>
        <input type="text" placeholder="Say something"
               value={this.state.text} onChange={this.handleTextChange}/>
        <input type="submit" value="Post"/>
      </form>
    )
  }
});

var CommentsWrapper = React.createClass({
  render: function() {
    return (
      <CommentBox pollInterval="1000" url="/dashboard/comments"/>
    )
  }
});

var CommentBox = React.createClass({
  getInitialState: function() {
    return { data: [], intervalID: null };
  },
  handleCommentSubmit: function(comment) {
    var comments = this.state.data;
    comment.id = Date.now();
    console.log(comment);
    var newComments = comments.concat([comment]);
    this.setState({ data: newComments });

    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    var data = { comment: comment, authenticity_token: csrf_token };
    
    $.ajax({
      url: this.props.url,
      data: data,
      dataType: 'json',
      method: 'POST',
      success: function(comments) {
        this.setState({ data: comments });
      }.bind(this)
    });
  },
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(comments) {
        if (this.isMounted()) {
          this.setState({ data: comments });
        } else {
          console.log('Component is not mounted anymore!');
          if (this.state.intervalID) {
            console.log('clearing the interval');
            clearInterval(this.state.intervalID);
          }
        }
      }.bind(this)
    });
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    var intervalID = setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    this.setState({ intervalID: intervalID });
  },
  render: function() {
    return (
      <div className="commentBox">
        <h1>Comments</h1>
        <CommentList data={this.state.data}/>
        <CommentForm onCommentSubmit={this.handleCommentSubmit}/>
      </div>
    );
  }
});

var browserHistory = ReactRouter.browserHistory;

var App = React.createClass({
  render: function() {
    return (
      <div>
        <h1>Dashboard</h1>
        <ul className="header">
          <li><IndexLink to="/dashboard" activeClassName="active">Home</IndexLink></li>
          <li><Link to="/dashboard/comments" activeClassName="active">Comments</Link></li>
        </ul>
        <div className="content">
          {this.props.children}
        </div>
      </div>
    )
  }
});

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/dashboard" component={App}>
      <IndexRoute component={Welcome} history={history}/>
      <Route path="/dashboard/comments" component={CommentsWrapper}/>
    </Route>
  </Router>,
  document.getElementById('content')
);
