class RenameFundIdInDonations < ActiveRecord::Migration[5.0]
  def change
    rename_column :donations, :fund_id, :funds
  end
end
