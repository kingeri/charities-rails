class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.references :charity, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true
      t.date :donation_date
      t.integer :fund_id
      t.timestamps null: false
    end
  end
end
