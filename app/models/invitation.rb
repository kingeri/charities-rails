class Invitation

  include ActiveModel::Model

  attr_writer :emails

  def emails
    @emails.to_a
  end

  def create
    puts "Creating..."
  end

end
