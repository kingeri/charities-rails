class Charity < ActiveRecord::Base
  has_many :donations
  has_many :users, through: :donations

  def self.charity_total_donations_by_user(user)
    joins(:donations).select('charities.*, sum(funds) as total').
    where('donations.user_id = ?', user.id).
    group('charities.id').order('total desc')
  end

  def self.charity_total_donations_by_company(company)
    joins(donations: :user).select('charities.*, sum(funds) as total').
    where('company_id = ?', company.id).
    group('charities.id').order('total desc')
  end
end
