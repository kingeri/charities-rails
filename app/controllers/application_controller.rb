class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!

  def require_company_rep!
    @my_company = Company.find_by(admin_id: current_user.id)
    redirect_to charities_path unless @my_company
  end

end
