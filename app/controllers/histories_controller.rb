class HistoriesController < ApplicationController

  def show
    @total_donations = current_user.donations.map(&:funds).sum
    @company_donations = current_user.company ? current_user.try(:company).donations.map(&:funds).sum : 0
    @donations_by_user = Charity.charity_total_donations_by_user(current_user)
    # turn into a hash for fast charity-total lookup
    @donations_by_user = Hash[@donations_by_user.map { |d| [ d.id, d.total ] }]

    @donations_by_company = Charity.charity_total_donations_by_company(current_user.company)
    # turn into a hash for fast charity-total lookup
    #@donations_by_company = Hash[@donations_by_company.map { |d| [ d.id, d.total ] }]
  end

end
