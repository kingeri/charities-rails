module Dashboard
  class CommentsController < ApplicationController

    layout 'react'

    def create
      Comment.create(comment_params)
      comments = Comment.order(:id)
      render json: comments
    end

    def index
      comments = Comment.order(:id)
      respond_to do |format|
        format.html
        format.json { render json: comments }
      end
    end

    private

    def comment_params
      params.require(:comment).permit(:author, :text)
    end

  end
end
