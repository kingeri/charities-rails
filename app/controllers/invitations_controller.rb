class InvitationsController < ApplicationController

  def new
    @invitation = Invitation.new(emails: [ '', '', '' ])
  end

  def create
    @invitation = Invitation.new(invitation_params)
    if @invitation.valid?
      @invitation.create
    else
      puts 1
    end
  end

  private

  def invitation_params
    params.require(:invitation).permit(emails: [])
  end

end
