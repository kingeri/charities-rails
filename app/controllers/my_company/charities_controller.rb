class MyCompany::CharitiesController < ApplicationController

  before_action :require_company_rep!

  def index
    my_user_donations = Donation.where(user_id: @my_company.users)
    donated_charities = my_user_donations.map(&:charity).uniq
    @charities = donated_charities.map { |c| present_charity(c) }
  end

  private

  def present_charity(charity)
    {
      charity: charity,
      total: charity.donations.map(&:fund_id).sum
    }
  end

end
