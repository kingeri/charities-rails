class CharitiesController < ApplicationController
  def index
    @charities = Charity.order('name')
    @additional_params = params[:date] ? { donation_date: params[:date] } : {}
  end
end
